<div class="box">
	<div class="title_box">Идиомы</div>
	<div class="content_box">
		<div class="text_content_block">
			<?php foreach ($idioms as $idiom) { ?>
				<div class="block">
					<p class="title"><?=$idiom->title?></p>
					<p class="description"><?=$idiom->full?></p>
				</div>
			<?php } ?>
			<div class="more">
				<ul>
					<li class="usual">
						<a href="/practice/idioms.html">Другие идиомы</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>