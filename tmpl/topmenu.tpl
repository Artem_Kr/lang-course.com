<?php function printItem($item, &$items, $childrens, $active) { ?>
	<?php if (count($items) == 0) return; ?>
	<li <?php if (in_array($item->id, $active)) { ?>class="active"<?php } ?>>
		<?php if ($item->parent_id == NULL) { ?>
			<a href="javascript:void(0);">
				<span><?=$item->title?></span>
				<i></i>
			</a>
		<?php } else { ?>
			<a <?php if ($item->external) { ?>rel="external"<?php } ?> href="<?=$item->link?>">
				<?=$item->title?>
			</a>
		<?php } ?>
		<?php if ($item->parent_id == NULL) { ?>
			<ul>
				<?php
					while(true) {
						$key = array_search($item->id, $childrens);
						if (!$key) break;
						unset($childrens[$key]);
				?>
					<?=printItem($items[$key], $items, $childrens, $active)?>
				<?php } ?>
			</ul>
		<?php } ?>
	</li>
<?php unset($items[$item->id]); } ?>
<nav class="topmenu" id="topmenu">
	<ul>
		<?php foreach ($items as $item) { ?>
			<?=printItem($item, $items, $childrens, $active)?>
		<?php } ?>
	</ul>
</nav>