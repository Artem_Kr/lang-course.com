<!DOCTYPE html>
<html>
<?=$header?>
<body>
	<div id="container">
		<header id="header">
			<div class="logo">
				<a href="/"></a>
			</div>
			<?=$top?>
			<?=$searchbt?>
		</header>
		<?=$auth?>
		<div id="content">
			<div class="right_boxes">
				<?=$right?>
			</div>
			<div class="center">
				<?=$center?>
			</div>
			<div class="clear"></div>
		</div>
		<footer>
			<?=$footmenu?>
			<div class="bottom">
				<div class="copyright">
					<p>All Right Reserved.<br />Copyright &copy; Progect Lang-Course.com, 2013-<?=date("Y")?></p>
				</div>
				<div class="statistic">

				</div>
			</div>
		</footer>
	</div>
</body>
</html>