<div class="box">
	<div class="title_box">Панель пользователя</div>
	<div class="content_box">
		<div class="user_panel_content">
			<p>Здравствуйте, <?=$user->name?>!</p>
			<div class="user_avatar">
				<img src="<?=$user->avatar?>" alt="<?=$user->name?>" class="big_avatar" />
			</div>
			<nav class="user_menu">
				<ul>
					<?php foreach ($items as $item) { ?>
						<li>
							<a <?php if ($item->link == $uri) { ?>class="current"<?php } ?> href="<?=$item->link?>"><?=$item->title?></a>
						</li>
					<?php } ?>
				</ul>
			</nav>
		</div>
	</div>
</div>