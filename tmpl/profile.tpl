<div class="main">
	<?php if (isset($hornav)) { ?><?=$hornav?><?php } ?>
	<div id="profile" class="tabs-block">
		<ul class="tabs">
			<li class="current" rel="?act=avatar">Аватар</li>
			<li rel="?act=basic_data">Основное</li>
			<li rel="?act=safety">Безопасность</li>
		</ul>
		<div class="box visible">
			<h1>Аватар</h1>
			<div class="avatar_info">
				<div class="avatar">
					<img src="<?=$avatar?>" alt="Аватар" />
				</div>
				<p>Вы можете загрузить изображения в формате <b>JPG</b>, <b>GIF</b> или <b>PNG</b>.<br />Размер изображения не должен превышать <b><?=$max_size?> КБ</b></p>
			</div>
			<?=$form_avatar?>
		</div>
		<div class="box">
			<?=$form_basic_user_data?>
		</div>
		<div class="box">
			<?=$form_user_safety?>
		</div>
	</div>
</div>