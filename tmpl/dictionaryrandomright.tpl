<div class="box">
	<div class="title_box">Англо-русский словарь</div>
	<div class="content_box">
		<div class="text_content_block">
			<?php foreach ($dictionarys as $dictionary) { ?>
				<div class="block">
					<p class="title"><?=$dictionary->title?></p>
					<p class="description"><?=$dictionary->full?></p>
				</div>
			<?php } ?>
			<div class="more">
				<ul>
					<li class="usual">
						<a href="/theory/english-russian-dictionary.html">Перевод других слов</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>