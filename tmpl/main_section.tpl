<div class="main">
	<div class="block_record" style="margin: 0;">
		<div class="block">
			<div class="content_block_categories">
				<nav class="menu_main_sections">
					<ul>
						<?php foreach ($sections as $section) { ?>
							<li>
								<a href="<?=$section->link?>"><?=($section->title)?></a>
							</li>
						<?php } ?>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>