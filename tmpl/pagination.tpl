<?php
	$count_pages = ceil($count_elements / $count_on_page);
	if ($count_pages > 1) {
		$left = $active - 1;
		$right = $count_pages - $active;
		if ($left < floor($count_show_pages / 2)) $start = 1;
		else $start = $active - floor($count_show_pages / 2);
		$end = $start + $count_show_pages - 1;
		if ($end > $count_pages) {
			$start -= ($end - $count_pages);
			$end = $count_pages;
			if ($start < 1) $start = 1;
		}
?>
	<div class="pagination">
		<ul>
			<?php if ($active != 1) { ?>
				<li><a href="<?=$url?>" title="Первая">Первая</a></li>
				<li><a href="<?php if ($active == 2) { ?><?=$url?><?php } else { ?><?=$url_page.($active - 1)?><?php } ?>" title="Предыдущая">Предыдущая</a></li>
			<?php } else { ?>
				<li><span class="current">Первая</span></li>
				<li><span class="current">Предыдущая</span></li>
			<?php } ?>
			<?php for ($i = $start; $i <= $end; $i++) { ?>
				<?php if ($i == $active) { ?>
					<li><span class="current"><?=$i?></span></li>
				<?php } else { ?>
					<li><a href="<?php if ($i == 1) { ?><?=$url?><?php } else { ?><?=$url_page.$i?><?php } ?>"><?=$i?></a></li>
				<?php } ?>
			<?php } ?>
			<?php if ($active != $count_pages) { ?>
				<li><a href="<?=$url_page.($active + 1)?>" title="Следующая">Следующая</a></li>
				<li><a href="<?=$url_page.$count_pages?>" title="Последняя">Последняя</a></li>
			<?php } else { ?>
				<li><span class="current">Следующая</span></li>
				<li><span class="current">Последняя</span></li>
			<?php } ?>
		</ul>
	</div>
<?php } ?>