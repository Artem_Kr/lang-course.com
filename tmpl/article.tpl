<?php if (isset($hornav)) { ?><?=$hornav?><?php } ?>
<div class="main inside_page">
	<div class="record_insode_page">
		<article>
			<h1><?=$article->title?></h1>
			<div class="additional">
				<div class="comments">
					<a href="#">
						<span class="num_comm">12 Комментариев</span>
					</a>
				</div>
				<div class="date">
					<span class="date_publication"><?=$article->day_show?> <?=$article->month_show?> <?=$article->year_show?></span>
				</div>
			</div>
			<?php if ($article->img) { ?>
				<div class="image_record">
					<img src="<?=$article->img?>" alt="<?=$article->title?>" />
				</div>
			<?php } ?>
			<?=$article->full?>
		</article>
		<div id="record_pn">
			<?php if ($prev_article) { ?><a id="prev_record" href="<?=$prev_article->link?>">ПРЕДЫДУЩАЯ СТАТЬЯ</a><?php } ?>
			<?php if ($next_article) { ?><a id="next_record" href="<?=$next_article->link?>">СЛЕДУЮЩАЯ СТАТЬЯ</a><?php } ?>
			<div class="clear"></div>
		</div>
		<div class="social_networks_and_adv_banners">
			<div class="social_networks">
			
			</div>
		</div>
	</div>
</div>