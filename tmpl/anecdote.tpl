<?php if (isset($hornav)) { ?><?=$hornav?><?php } ?>
<div class="main inside_page">
	<div class="record_insode_page">
		<article>
			<h1><?=$anecdote->title?></h1>
			<div class="additional">
				<div class="comments">
					<a href="#">
						<span class="num_comm">12 Комментариев</span>
					</a>
				</div>
			</div>
			<?=$anecdote->full?>
		</article>
		<div id="record_pn">
			<?php if ($prev_anecdote) { ?><a id="prev_record" href="<?=$prev_anecdote->link?>">ПРЕДЫДУЩИЙ АНЕКДОТ</a><?php } ?>
			<?php if ($next_anecdote) { ?><a id="next_record" href="<?=$next_anecdote->link?>">СЛЕДУЮЩИЙ АНЕКДОТ</a><?php } ?>
			<div class="clear"></div>
		</div>
	</div>
</div>