<div class="footmenu">
	<nav>
		<ul>
			<?php foreach ($items as $item) { ?>
				<li>
					<a href="<?=$item->link?>" <?php if ($item->external) { ?>rel="external"<?php } ?>><?=$item->title?></a>
				</li>
			<?php } ?>
		</ul>
	</nav>
</div>