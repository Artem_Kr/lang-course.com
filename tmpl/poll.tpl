<div class="box">
	<div class="title_box">Опрос</div>
	<div class="content_box">
		<div class="poll">
			<p><b><?=$title?></b></p>
			<form name="poll" action="<?=$action?>" method="post" />
				<?php foreach ($data as $d) { ?>
					<div>
						<input id="poll_data_<?=$d->id?>" class="radio_button" name="poll_data_id" type="radio" value="<?=$d->id?>" />
						<label for="poll_data_<?=$d->id?>"><?=$d->title?></label>
					</div>
				<?php } ?>
				<div>
					<input class="yellow_button_small" name="poll" type="submit" value="ГОЛОСОВАТЬ">
				</div>
			</form>
		</div>
	</div>
</div>