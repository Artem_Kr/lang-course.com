<div class="main">
	<?php if ($categories) { ?>
		<div class="block_record" style="margin: 20px 0 0;">
			<div class="block">
				<div class="content_block_categories">
					<nav class="menu_categories_section">
						<ul>
							<?php foreach ($categories as $category) { ?>
								<li>
									<a href="<?=$category->link?>"><?=mb_strtoupper($category->title)?></a>
								</li>
							<?php } ?>
						</ul>
					</nav>
				</div>
			</div>
			<?php if ($dictionarys) { ?>
				<div class="block">
					<div class="content_block">
						<?php foreach ($dictionarys as $dictionary) { ?>
							<section>
								<div class="intro_text">
									<h2><?=$dictionary->title?></h2>
									<p><?=$dictionary->full?></p>
								</div>
								<div class="clear"></div>
							</section>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
			<?php if ($idioms) { ?>
				<div class="block">
					<div class="content_block">
						<?php foreach ($idioms as $idiom) { ?>
							<section>
								<div class="intro_text">
									<h2><?=$idiom->title?></h2>
									<p><?=$idiom->full?></p>
									<p>
										<b>Example:</b>
										<br />
										<span class="example"><?=$idiom->example?></span>
									</p>
								</div>
								<div class="clear"></div>
							</section>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
	<?php } ?>

	<?php if ($articles) { ?>
		<div class="block_record" style="margin: 20px 0 0;">
			<div class="block">
				<div class="content_block">
					<?php foreach ($articles as $article) { ?>
						<section>
							<div class="image">
								<img src="<?=$article->img?>" alt="<?=$article->title?>">
							</div>
							<div class="intro_text">
								<h2><?=$article->title?></h2>
								<p><?=$article->intro?></p>
							</div>
							<div class="clear"></div>
							<div class="additional">
								<div class="comments">
									<a href="#">
										<span class="num_comm">12 Комментариев</span>
									</a>
								</div>
								<div class="date">
									<span class="date_publication"><?=$article->day_show?> <?=$article->month_show?> <?=$article->year_show?></span>
								</div>
							</div>
							<div class="more">
								<a href="<?=$article->link?>" class="yellow_button_large">Прочитать</a>
							</div>
						</section>
					<?php } ?>
				</div>
				<?=$pagination?>
			</div>
		</div>
	<?php } ?>

	<?php if ($anecdotes) { ?>
		<div class="block_record" style="margin: 20px 0 0;">
			<div class="block">
				<div class="content_block">
					<?php foreach ($anecdotes as $anecdote) { ?>
						<section>
							<div class="intro_text">
								<h2><?=$anecdote->title?></h2>
							</div>
							<div class="clear"></div>
							<div class="additional">
								<div class="comments">
									<a href="#">
										<span class="num_comm">2 Комментария</span>
									</a>
								</div>
							</div>
							<div class="more">
								<a href="<?=$anecdote->link?>" class="yellow_button_large">Прочитать</a>
							</div>
						</section>
					<?php } ?>
				</div>
				<?=$pagination?>
			</div>
		</div>
	<?php } ?>
</div>