<div class="main">
	<?php if ($section_id->id == 2) { ?>
		<?php if (count($dictionarys)) { ?>
			<div class="block_record" style="margin: 0;">
				<div class="block">
					<div class="content_block">
						<?php foreach ($dictionarys as $dictionary) { ?>
							<section>
								<div class="intro_text">
									<h2><?=$dictionary->title?></h2>
									<p><?=$dictionary->full?></p>
								</div>
								<div class="clear"></div>
							</section>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } else { ?>
			<p>В данную категорию еще не добавлено ни одного слова.</p>
		<?php } ?>
	<?php } ?>

	<?php if ($section_id->id == 5) { ?>
		<?php if (count($idioms)) { ?>
			<div class="block_record" style="margin: 0;">
				<div class="block">
					<div class="content_block">
						<?php foreach ($idioms as $idiom) { ?>
							<section>
								<div class="intro_text">
									<h2><?=$idiom->title?></h2>
									<p><?=$idiom->full?></p>
									<p>
										<b>Example:</b>
										<br />
										<span class="example"><?=$idiom->example?></span>
									</p>
								</div>
								<div class="clear"></div>
							</section>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } else { ?>
			<p>В данную категорию еще не добавлено ни одной идиомы.</p>
		<?php } ?>
	<?php } ?>
</div>	