<!-- Auth Block -->
	<div id="auth_block_fix">
		<div id="auth_block">
			<div>
				<?php if ($message) { ?>
					<script>noty({text: '<?=$message?>', type: "error"});</script>
				<?php } ?>
				<form name="auth" action="<?=$action?>" method="post">
					<div>
						<input class="input_text" name="login" type="text" placeholder="логин">
						<input class="input_text" name="password" type="password" placeholder="пароль">
						<input class="yellow_button_small" name="auth" type="submit" value="ВОЙТИ">
					</div>
				</form>
				<a class="green_button_small" href="<?=$link_register?>">РЕГИСТРАЦИЯ</a>
				<div class="reset">
					<a href="<?=$link_reset?>">Забыли пароль?</a>
				</div>
			</div>
		</div>
	</div>
<!-- End Auth Block -->