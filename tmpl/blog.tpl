<div class="block">
	<div class="title_block">Новые статьи</div>
	<div class="content_block">
		<?php if (count($articles)) { ?>
			<?php foreach ($articles as $article) { ?>
				<section>
					<div class="image">
						<img src="<?=$article->img?>" alt="<?=$article->title?>">
					</div>
					<div class="intro_text">
						<h2><?=$article->title?></h2>
						<p><?=$article->intro?></p>
					</div>
					<div class="clear"></div>
					<div class="additional">
						<div class="comments">
							<a href="#">
								<span class="num_comm">12 Комментариев</span>
							</a>
						</div>
						<div class="date">
							<span class="date_publication"><?=$article->day_show?> <?=$article->month_show?> <?=$article->year_show?></span>
						</div>
					</div>
					<div class="more">
						<a href="<?=$article->link?>" class="yellow_button_large">Прочитать</a>
					</div>
				</section>
			<?php } ?>
		<?php } else { ?>
			<h4>В разделе нет статей.</h4>
		<?php } ?>
	</div>
</div>
<div class="block">
	<div class="title_block">Новые Анекдоты</div>
	<div class="content_block">
		<?php if (count($anecdotes)) { ?>
			<?php foreach ($anecdotes as $anecdote) { ?>
				<section>
					<div class="intro_text">
						<h2><?=$anecdote->title?></h2>
					</div>
					<div class="clear"></div>
					<div class="additional">
						<div class="comments">
							<a href="#">
								<span class="num_comm">2 Комментария</span>
							</a>
						</div>
					</div>
					<div class="more">
						<a href="<?=$anecdote->link?>" class="yellow_button_large">Прочитать</a>
					</div>
				</section>
			<?php } ?>
		<?php } else { ?>
			<h4>В разделе нет анекдотов.</h4>
		<?php } ?>
	</div>
</div>