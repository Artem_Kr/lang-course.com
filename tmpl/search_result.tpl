<?php
	function getWord($number, $suffix) {
		$keys = array(2, 0, 1, 1, 1, 2);
		$mod = $number % 100;
		$suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
		return $suffix[$suffix_key];
	}

	$array = array("запись", "записи", "записей");
	$found_queries = count($data_1) + count($data_2) + count($data_4) + count($data_5);
	$word = getWord($found_queries, $array);
?>

<?=$hornav?>
<div class="main search_inside_page search_page">
	<form name="searchForm" action="<?=$link_search?>" method="get">
		<input class="input_text input_text_large" name="query" type="text" value="<?=$query?>" placeholder="введите текст для поиска...">
		<input class="yellow_button_large" type="submit" value="ИСКАТЬ">
	</form>
	<?php if ($error_len) { ?>
		<p class="message">Слишком короткий поисковый запрос!</p>
	<?php } else { ?>
		<div class="search_result">
			<?php if ($found_queries == 0) { ?>
				<p class="find_text">По вашему запросу ничего не найдено.</p>
			<?php } else { ?>
				<p class="find_text">Найдено: <b><?=$found_queries?></b> <?=$word?></p>
			<?php } ?>
			<?php foreach ($data_1 as $d_1) { ?>
				<div class="search_item">
					<div class="info_item">
						<ul>
							<li><a href="<?=$d_1->link?>" class="title"><?=$d_1->title?></a></li>
							<?php if (isset($d_1->section) || isset($d_1->category)) { ?>
								<li>
									<?=$d_1->main_section->title?> / <?=$d_1->section->title?>
								</li>
							<?php } ?>
						</ul>
						<div class="clear"></div>
					</div>
					<div class="text_item">
						<p><?=$d_1->description?></p>
					</div>
				</div>
			<?php } foreach ($data_2 as $d_2) { ?>
				<div class="search_item">
					<div class="info_item">
						<ul>
							<li><a href="<?=$d_2->link?>" class="title"><?=$d_2->title?></a></li>
							<?php if (isset($d_2->section) || isset($d_2->category)) { ?>
								<li>
									<?=$d_2->main_section->title?> / <?=$d_2->section->title?>
								</li>
							<?php } ?>
						</ul>
						<div class="clear"></div>
					</div>
					<div class="text_item">
						<p><?=$d_2->description?></p>
					</div>
				</div>
			<?php } ?>


			<?php foreach ($data_4 as $d_4) { ?>
				<div class="search_item">
					<div class="info_item">
						<ul>
							<li><span class="title"><?=$d_4->title?></span></li>
							<?php if (isset($d_4->section) || isset($d_4->category)) { ?>
								<li>
									<?=$d_4->main_section->title?> / <?=$d_4->section->title?><?php if ($d_4->category) { ?> / <?=$d_4->category->title?><?php } ?>
								</li>
							<?php } ?>
						</ul>
						<div class="clear"></div>
					</div>
					<div class="text_item">
						<p><?=$d_4->description?></p>
					</div>
				</div>
			<?php } foreach ($data_5 as $d_5) { ?>
				<div class="search_item">
					<div class="info_item">
						<ul>
							<li><span class="title"><?=$d_5->title?></span></li>
							<?php if (isset($d_5->section) || isset($d_5->category)) { ?>
								<li>
									<?=$d_5->main_section->title?> / <?=$d_5->section->title?><?php if ($d_5->category) { ?> / <?=$d_5->category->title?><?php } ?>
								</li>
							<?php } ?>
						</ul>
						<div class="clear"></div>
					</div>
					<div class="text_item">
						<p><?=$d_5->description?></p>
					</div>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
</div>