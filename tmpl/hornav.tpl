<div id="header_navigation">
	<nav>
		<p><b>Вы здесь:</b>
			<?php $first = true; foreach ($data as $d) { ?>
			<?php if (!$first) { ?> &ndash; <?php } ?>
			<?php if ($d->link) { ?><a href="<?=$d->link?>"><?=$d->title?></a><?php } else { ?><?=$d->title?><?php } ?>
			<?php $first = false; } ?>
		</p>
	</nav>
</div>