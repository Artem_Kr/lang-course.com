$(document).ready(function() {
	// Topmenu
	$("#topmenu > ul > li").on("click", function () {
		var basicItem = $("#topmenu > ul > li"),
			classBasicItem = "active",
			idleTimer = null,
			idleState = false,	// состояние отсутствия
			idleWait = 2000;	// время ожидания 5 минут

		$(this).toggleClass(classBasicItem);

		basicItem.on("mouseenter", function () {
			if (basicItem.hasClass(classBasicItem)) {
				basicItem.removeClass(classBasicItem);
				$(this).toggleClass(classBasicItem);
			}
		});

		$(document).click(function (event) {
			if ($(event.target).closest("#topmenu").length) return;
			basicItem.removeClass(classBasicItem);
		});
		
		$(document).bind('mousemove keydown scroll', function () {
			clearTimeout(idleTimer); // отменяем прежний временной отрезок
//			if (idleState == true) {
//				// Действия на возвращение пользователя
//			}

			idleState = false;
			idleTimer = setTimeout(function () {
				// Если пользователь отсутствует в течении 5 минут, то закрываем открытый пункт меню
				basicItem.removeClass(classBasicItem);
				idleState = true;
			}, idleWait);
		});
	});
	
	// Auth Block
	$(window).resize(function () {
		var wAuthBlock = $("#auth_block_fix"),
			wContentBlock = $("#content").outerWidth();

		wAuthBlock.width(wContentBlock);
	}).resize();

	$("#show_auth_block").click(function () {
		$(this).toggleClass("active");
		$("#auth_block").toggleClass("show");
	});

	// Update Captcha
	$(".captcha img:last-child").bind("click", function (event) {
		var captcha = $(".captcha img:first-child"),
			src = $(captcha).attr("src"),
			i;

		if ((i = src.indexOf("?")) == -1) src += "?" + Math.random();
		else src = src.substring(0, i) + "?" + Math.random();
		$(captcha).attr("src", src);
	});

	// LocalStorage Tabs
	$(".tabs-block > .tabs").each(function (i) {
		var storage = localStorage.getItem("tab" + i);

		if (storage) $(this).find("li").eq(storage).addClass("current").siblings().removeClass("current")
			.parents(".tabs-block").find("div.box").hide().eq(storage).show();
	});

	$(".tabs-block > .tabs").on("click", "li:not(.current)", function () {
		$(this).addClass("current").siblings().removeClass("current").parents(".tabs-block").find("div.box").eq($(this).index()).fadeIn(150).siblings("div.box").hide();
		var ulIndex = $(".tabs-block > .tabs").index($(this).parents(".tabs-block > .tabs"));
		localStorage.removeItem("tab" + ulIndex);
		localStorage.setItem("tab" + ulIndex, $(this).index());
	});
})

$.noty.defaults = {
	layout: 'bottomLeft',
	theme: 'defaultTheme',
	type: 'alert',
	text: '', // может быть html кодом или строкой
	dismissQueue: true, // Если вы хотите использовать функцию очереди, установите значение true
	template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
	animation: {
		open: {
			height: 'toggle'
		},
		close: {
			height: 'toggle'
		},
		easing: 'swing',
		speed: 500 // скорость появления и исчезновения уведомления
	},
	timeout: 5000, // задержка перед закрытием уведомления. Установите false для липких уведомлений
	force: false, // добавляет уведомление в начало очереди, если установлено true
	modal: false,
	maxVisible: 5, // вы можете установить максимальную видимость уведомления для удаления из очереди
	killer: false, // для закрытия всех уведомлений до показа
	closeWith: ['click'], // ['click', 'button', 'hover']
	callback: {
		onShow: function () {},
		afterShow: function () {},
		onClose: function () {},
		afterClose: function () {}
	},
	buttons: false // массив кнопок
};