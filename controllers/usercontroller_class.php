<?php

class UserController extends Controller {

	public function actionEditProfile() {
		$message_avatar_name = "avatar";
		$message_basic_user_data = "basic_user_data";
		$message_user_safety = "user_safety";
		
		if ($this->request->change_avatar) {
			$img = $this->fp->uploadIMG($message_avatar_name, $_FILES["avatar"], Config::MAX_SIZE_AVATAR, Config::DIR_AVATAR);
			if ($img) {
				$tmp = $this->auth_user->getAvatar();
				$obj = $this->fp->process($message_avatar_name, $this->auth_user, array(array("avatar", $img)), array(), "SUCCESS_AVATAR_CHANGE");
				if ($obj instanceof UserDB) {
					if ($tmp) File::delete(Config::DIR_AVATAR.$tmp);
					$this->redirect(URL::current());
				}
			}
		}
		elseif ($this->request->basic_user_data) {
			$checks = array(array($this->auth_user->checkPassword($this->request->password_current_basic_user_data), true, "ERROR_PASSWORD_CURRENT"));
			$user_temp = $this->fp->process($message_basic_user_data, $this->auth_user, array("name"), $checks, "SUCCESS_BASIC_USER_DATA_CHANGE");
			if ($user_temp instanceof UserDB) $this->redirect(URL::current());
		}
		elseif ($this->request->user_safety) {
			$checks = array(array($this->auth_user->checkPassword($this->request->password_current), true, "ERROR_PASSWORD_CURRENT"));
			$checks[] = array($this->request->password, $this->request->password_conf, "ERROR_PASSWORD_CONF");
			$user_temp = $this->fp->process($message_user_safety, $this->auth_user, array(array("setPassword()", $this->request->password)), $checks, "SUCCESS_PASSWORD_CHANGE");
			if ($user_temp instanceof UserDB) {
				$this->auth_user->login();
				$this->redirect(URL::current());
			}
		}
		
		$this->title = "Редактирование профиля";
		$this->meta_desc = "Редактирование профиля пользователя.";
		$this->meta_key = "редактирование профиля, редактирование профиля пользователя, редактирование профиля пользователя сайт";
		
		$form_avatar = new Form();
		$form_avatar->name = "change_avatar";
		$form_avatar->action = URL::current();
		$form_avatar->enctype = "multipart/form-data";
		$form_avatar->message = $this->fp->getSessionMessage($message_avatar_name);
		$form_avatar->file("avatar", "Аватар");
		$form_avatar->submit("СОХРАНИТЬ");
		
		$form_avatar->addJSV("avatar", $this->jsv->avatar());
		
		$form_basic_user_data = new Form();
		$form_basic_user_data->name = "basic_user_data";
		$form_basic_user_data->header = "Основное";
		$form_basic_user_data->action = URL::current();
		$form_basic_user_data->message = $this->fp->getSessionMessage($message_basic_user_data);
		$form_basic_user_data->text("name", "Ваше имя:", $this->auth_user->name);
		$form_basic_user_data->password("password_current_basic_user_data", "Текущий пароль:");
		$form_basic_user_data->submit("СОХРАНИТЬ");
		
		$form_basic_user_data->addJSV("name", $this->jsv->name());
		$form_basic_user_data->addJSV("password_current_basic_user_data", $this->jsv->password(false, false, "ERROR_PASSWORD_CURRENT_EMPTY"));

		$form_user_safety = new Form();
		$form_user_safety->name = "user_safety";
		$form_user_safety->header = "Безопасность";
		$form_user_safety->action = URL::current();
		$form_user_safety->message = $this->fp->getSessionMessage($message_user_safety);
		$form_user_safety->password("password_current", "Старый пароль:");
		$form_user_safety->password("password", "Новый пароль:");
		$form_user_safety->password("password_conf", "Повторите пароль:");
		$form_user_safety->submit("СОХРАНИТЬ");
		
		
		$form_user_safety->addJSV("password", $this->jsv->password("password_conf"));
		$form_user_safety->addJSV("password_current", $this->jsv->password(false, false, "ERROR_OLD_PASSWORD_EMPTY"));
		$hornav = $this->getHornav();
		$hornav->addData("Редактирование профиля");
		
		$this->render($this->renderData(array("hornav" => $hornav, "form_avatar" => $form_avatar, "form_basic_user_data" => $form_basic_user_data, "form_user_safety" => $form_user_safety), "profile", array("avatar" => $this->auth_user->avatar, "max_size" => (Config::MAX_SIZE_AVATAR / KB_B))));
	}

	public function actionControlPanel() {
		$this->title = "Панель управления";
		$this->meta_desc = "Панель управления сайтом lang-Course.com";
		$this->meta_key = "панель управления, панель управления сайтом";

		$hornav = $this->getHornav();
		$hornav->addData("Панель управления");

		$this->renderControlPanel($this->renderData(array("hornav" => $hornav), "c_panel_index"));
	}
	
	protected function access() {
		if ($this->auth_user) return true;
		return false;
	}
	
}

?>