<?php

class MainController extends Controller {

	public function actionIndex() {
		$this->title = "Английский язык";
		$this->meta_desc = "Английский язык";
		$this->meta_key = "английский язык";

		$articles = ArticleDB::getAllShow(Config::COUNT_RECORD_ON_PAGE_INDEX, $this->getOffset(Config::COUNT_RECORD_ON_PAGE_INDEX), true);
		$anecdotes = AnecdoteDB::getAllShow(Config::COUNT_RECORD_ON_PAGE_INDEX, $this->getOffset(Config::COUNT_RECORD_ON_PAGE_INDEX), true);
		$blog = new Blog();
		$blog->articles = $articles;
		$blog->anecdotes = $anecdotes;
		$this->render($this->renderData(array("blog" => $blog), "index"));
	}

	public function actionMainSection() {
		$mainsection_db = new MainSectionDB();
		$mainsection_db->load($this->request->id);
		if (!$mainsection_db->isSaved()) $this->notFound();
		$this->section_id = $mainsection_db->id;
		$this->title = $mainsection_db->title;
		$this->meta_desc = $mainsection_db->meta_desc;
		$this->meta_key = $mainsection_db->meta_key;
		
		$hornav = $this->getHornav();
		$hornav->addData($mainsection_db->title);

		$intro = new Intro();
		$intro->hornav = $hornav;
		$intro->obj = $mainsection_db;

		$main_section = new MainSection();

		$sections = SectionDB::getAllOnMainSectionID($this->request->id, false);
		$main_section->sections = $sections;

		$this->render($intro.$main_section);
	}

	public function actionSection() {
		$section_db = new SectionDB();
		$section_db->load($this->request->id);
		if (!$section_db->isSaved()) $this->notFound();
		$this->section_id = $section_db->id;
		$this->title = $section_db->title;
		$this->meta_desc = $section_db->meta_desc;
		$this->meta_key = $section_db->meta_key;

		$mainsection_db = new MainSectionDB();
		$mainsection_db->load($section_db->main_section_id);
		
		$hornav = $this->getHornav();
		$hornav->addData($mainsection_db->title, $mainsection_db->link);
		$hornav->addData($section_db->title);

		$search_form = '<div class="search_inside_page" style="margin: 0 0 10px;">
							<p><b>Воспользуйтесь поиском для того чтобы найти интересующие вас слова:</b></p>
							<form name="searchForm" action="/search.html" method="get">
								<input class="input_text input_text_large" name="query" type="text" value="" placeholder="введите текст для поиска...">
								<input class="yellow_button_large" type="submit" value="ИСКАТЬ">
							</form>
						</div>';

		$intro = new Intro();
		$intro->hornav = $hornav;
		$intro->obj = $section_db;
		$intro->search_form = $search_form;

		
		$section_record = new SectionRecord();

		if ($section_db->id == 2 OR $section_db->id == 5) {
			$categories = CategoryDB::getAllOnSectionID($this->request->id, false);
			$section_record->categories = $categories;

			if ($section_db->id == 2) {
				$dictionarys = DictionaryDB::getAllShow(Config::COUNT_FIFTEEN_RECORD_ON_PAGE, $this->getOffset(Config::COUNT_FIFTEEN_RECORD_ON_PAGE), true);
				$section_record->dictionarys = $dictionarys;
			}

			if ($section_db->id == 5) {
				$idioms = IdiomDB::getAllShow(Config::COUNT_FIFTEEN_RECORD_ON_PAGE, $this->getOffset(Config::COUNT_FIFTEEN_RECORD_ON_PAGE), true);
				$section_record->idioms = $idioms;
			}
		}

		if ($section_db->id == 3) {
			$articles = ArticleDB::getAllShow(Config::COUNT_TEN_RECORD_ON_PAGE, $this->getOffset(Config::COUNT_TEN_RECORD_ON_PAGE), true);
			$pagination_articles = $this->getPagination(ArticleDB::getCount(), Config::COUNT_TEN_RECORD_ON_PAGE, "/theory/articles.html");

			$section_record->articles = $articles;
			$section_record->pagination = $pagination_articles;
		}

		if ($section_db->id == 4) {
			$anecdotes = AnecdoteDB::getAllShow(Config::COUNT_TEN_RECORD_ON_PAGE, $this->getOffset(Config::COUNT_TEN_RECORD_ON_PAGE), true);
			$pagination_anecdotes = $this->getPagination(AnecdoteDB::getCount(), Config::COUNT_TEN_RECORD_ON_PAGE, "/practice/anecdotes.html");

			$section_record->anecdotes = $anecdotes;
			$section_record->pagination = $pagination_anecdotes;
		}

		$this->render($intro.$section_record);
	}

	public function actionCategory() {
		$category_db = new CategoryDB();
		$category_db->load($this->request->id);
		if (!$category_db->isSaved()) $this->notFound();
		$this->section_id = $category_db->section_id;
		$this->title = "Слова на букву \"".$category_db->title."\"";
		$this->meta_desc = $category_db->meta_desc;
		$this->meta_key = $category_db->meta_key;

		$mainsection_db = new MainSectionDB();
		$section_db = new SectionDB();
		$mainsection_db->load($category_db->main_section_id);
		$section_db->load($category_db->section_id);

		$hornav = $this->getHornav();
		$hornav->addData($mainsection_db->title, $mainsection_db->link);
		$hornav->addData($section_db->title, $section_db->link);
		$hornav->addData("Слова на букву \"".$category_db->title."\"");

		$search_form = '<div class="search_inside_page">
							<p><b>Воспользуйтесь поиском для того чтобы найти интересующие вас слова:</b></p>
							<form name="searchForm" action="/search.html" method="get">
								<input class="input_text input_text_large" name="query" type="text" value="" placeholder="введите текст для поиска...">
								<input class="yellow_button_large" type="submit" value="ИСКАТЬ">
							</form>
						</div>';

		$intro = new Intro();
		$intro->hornav = $hornav;
		$intro->obj = $category_db;
		$intro->search_form = $search_form;

		$category = new Category();

		$category->section_id = $section_db;

		if ($section_db->id == 2) {
			$dictionarys = DictionaryDB::getAllOnPageAndCategoryID($this->request->id, Config::COUNT_TEN_RECORD_ON_PAGE);
			$category->dictionarys = $dictionarys;
		}

		if ($section_db->id == 5) {
			$idioms = IdiomDB::getAllOnPageAndCategoryID($this->request->id, Config::COUNT_TEN_RECORD_ON_PAGE);
			$category->idioms = $idioms;
		}

		$this->render($intro.$category);
	}

	public function actionArticle() {
		$article_db = new ArticleDB();
		$article_db->load($this->request->id);
		if (!$article_db->isSaved()) $this->notFound();
		$this->title = $article_db->title;
		$this->meta_desc = $article_db->meta_desc;
		$this->meta_key = $article_db->meta_key;
		
		$hornav = $this->getHornav();
		
		if ($article_db->main_section) {
			$this->main_section_id = $article_db->main_section->id;
			$hornav->addData($article_db->main_section->title, $article_db->main_section->link);
			$this->url_active  = URL::get("mainsection", "", array("id" => $article_db->main_section->id));
		}
		if ($article_db->section) {
			$this->section_id = $article_db->section->id;
			$hornav->addData($article_db->section->title, $article_db->section->link);
			$this->url_active  = URL::get("section", "", array("id" => $article_db->section->id));
		}
		
		$hornav->addData($article_db->title);
		
		$prev_article_db = new ArticleDB();
		$prev_article_db->loadPrevArticle($article_db);
		$next_article_db = new ArticleDB();
		$next_article_db->loadNextArticle($article_db);
		
		$article = new Article();
		$article->hornav = $hornav;
		$article->article = $article_db;
		if ($prev_article_db->isSaved()) $article->prev_article = $prev_article_db;
		if ($next_article_db->isSaved()) $article->next_article = $next_article_db;
		
		$this->render($article);
	}

	public function actionAnecdote() {
		$anecdote_db = new AnecdoteDB();
		$anecdote_db->load($this->request->id);
		if (!$anecdote_db->isSaved()) $this->notFound();
		$this->title = $anecdote_db->title;
		$this->meta_desc = $anecdote_db->meta_desc;
		$this->meta_key = $anecdote_db->meta_key;
		
		$hornav = $this->getHornav();
		
		if ($anecdote_db->main_section) {
			$this->main_section_id = $anecdote_db->main_section->id;
			$hornav->addData($anecdote_db->main_section->title, $anecdote_db->main_section->link);
			$this->url_active  = URL::get("mainsection", "", array("id" => $anecdote_db->main_section->id));
		}
		if ($anecdote_db->section) {
			$this->section_id = $anecdote_db->section->id;
			$hornav->addData($anecdote_db->section->title, $anecdote_db->section->link);
			$this->url_active  = URL::get("section", "", array("id" => $anecdote_db->section->id));
		}
		
		$hornav->addData($anecdote_db->title);
		
		$prev_anecdote_db = new AnecdoteDB();
		$prev_anecdote_db->loadPrevAnecdote($anecdote_db);
		$next_anecdote_db = new AnecdoteDB();
		$next_anecdote_db->loadNextAnecdote($anecdote_db);
		
		$anecdote = new Anecdote();
		$anecdote->hornav = $hornav;
		$anecdote->anecdote = $anecdote_db;
		if ($prev_anecdote_db->isSaved()) $anecdote->prev_anecdote = $prev_anecdote_db;
		if ($next_anecdote_db->isSaved()) $anecdote->next_anecdote = $next_anecdote_db;
		
		$this->render($anecdote);
	}

	public function actionPage() {
		$page_db = new PageDB();
		$page_db->load($this->request->id);
		if (!$page_db->isSaved()) $this->notFound();
		$this->title = $page_db->title;
		$this->meta_desc = $page_db->meta_desc;
		$this->meta_key = $page_db->meta_key;
		
		$hornav = $this->getHornav();
		
		$hornav->addData($page_db->title);
		
		$page = new Page();
		$page->hornav = $hornav;
		$page->page = $page_db;
		
		$this->render($page);
	}

	public function actionPoll() {
		$message_name = "poll";
		if ($this->request->poll) {
			$poll_voter_db = new PollVoterDB();
			$poll_data = PollDataDB::getAllOnPollID($this->request->id);
			$already_poll = PollVoterDB::isAlreadyPoll(array_keys($poll_data));
			$checks = array(array($already_poll, false, "ERROR_ALREADY_POLL"));
			$this->fp->process($message_name, $poll_voter_db, array("poll_data_id"), $checks, "SUCCESS_POLL");
			$this->redirect(URL::current());
		}
		$poll_db = new PollDB();
		$poll_db->load($this->request->id);
		if (!$poll_db->isSaved()) $this->notFound();
		$this->title = "Результаты голосования: ".$poll_db->title;
		$this->meta_desc = "Результаты голосования: ".$poll_db->title.".";
		$this->meta_key = "результаты голосования, ".mb_strtolower($poll_db->title);
		
		$poll_data = PollDataDB::getAllDataOnPollID($poll_db->id);
		
		$hornav = $this->getHornav();
		$hornav->addData($poll_db->title);
		
		$poll_result = new PollResult();
		$poll_result->hornav = $hornav;
		$poll_result->message = $this->fp->getSessionMessage($message_name);
		$poll_result->title = $poll_db->title;
		$poll_result->data = $poll_data;
		
		$this->render($poll_result);
	}

	public function actionRegister() {
		$message_name = "register";
		if ($this->request->register) {
			$user_old_1 = new UserDB();
			$user_old_1->loadOnEmail($this->request->email);
			$user_old_2 = new UserDB();
			$user_old_2->loadOnLogin($this->request->login);
			$captcha = $this->request->captcha;
			$checks = array(array(Captcha::check($captcha), true, "ERROR_CAPTCHA_CONTENT"));
			$checks[] = array($this->request->password, $this->request->password_conf, "ERROR_PASSWORD_CONF");
			$checks[] = array($user_old_1->isSaved(), false, "ERROR_EMAIL_ALREADY_EXISTS");
			$checks[] = array($user_old_2->isSaved(), false, "ERROR_LOGIN_ALREADY_EXISTS");
			$user = new UserDB();
			$fields = array("name", "login", "email", array("setPassword()", $this->request->password));
			$user = $this->fp->process($message_name, $user, $fields, $checks);
			if ($user instanceof UserDB) {
				$this->mail->send($user->email, array("user" => $user, "link" => URL::get("activate", "", array("login" => $user->login, "key" => $user->activation), false, Config::ADDRESS)), "register");
				$this->redirect(URL::get("sregister"));
			}
		}
		$this->title = "Регистрация на сайте ".Config::SITENAME;
		$this->meta_desc = "Регистрация на сайте ".Config::SITENAME.".";
		$this->meta_key = "регистрация сайт ".mb_strtolower(Config::SITENAME).", зарегистрироваться сайт ".mb_strtolower(Config::SITENAME);
		$hornav = $this->getHornav();
		$hornav->addData("Регистрация");
		
		$form = new Form();
		$form->hornav = $hornav;
		$form->header = "Регистрация";
		$form->name = "register";
		$form->action = URL::current();
		$form->message = $this->fp->getSessionMessage($message_name);
		$form->text("name", "Ваше имя:", $this->request->name);
		$form->text("login", "Логин:", $this->request->login);
		$form->text("email", "E-mail:", $this->request->email);
		$form->password("password", "Пароль:");
		$form->password("password_conf", "Подтвердите пароль:");
		$form->captcha("captcha", "Введите код с картинки:");
		$form->submit("РЕГИСТРАЦИЯ");
		
		$form->addJSV("name", $this->jsv->name());
		$form->addJSV("login", $this->jsv->login());
		$form->addJSV("email", $this->jsv->email());
		$form->addJSV("password", $this->jsv->password("password_conf"));
		$form->addJSV("captcha", $this->jsv->captcha());
		
		$this->render($form);
	}

	public function actionSRegister() {
		$this->title = "Регистрация на сайте ".Config::SITENAME;
		$this->meta_desc = "Регистрация на сайте ".Config::SITENAME.".";
		$this->meta_key = "регистрация сайт ".mb_strtolower(Config::SITENAME).", зарегистрироваться сайт ".mb_strtolower(Config::SITENAME);
	
		$hornav = $this->getHornav();
		$hornav->addData("Регистрация");
		
		$pm = new PageMessage();
		$pm->hornav = $hornav;
		$pm->header = "Регистрация";
		$pm->text = "Учётная запись создана. На указанный Вами адрес электронной почты отправлено письмо с инструкцией по активации. Если письмо не доходит, то обратитесь к администрации.";
		$this->render($pm);
	}

	public function actionActivate() {
		$user_db = new UserDB();
		$user_db->loadOnLogin($this->request->login);
		$hornav = $this->getHornav();
		if ($user_db->isSaved() && ($user_db->activation == "")) {
			$this->title = "Ваш аккаунт уже активирован";
			$this->meta_desc = "Вы можете войти в свой аккаунт, используя Ваши логин и пароль.";
			$this->meta_key = "активация, успешная активация, успешная активация регистрация";
			$hornav->addData("Активация");
		}
		elseif ($user_db->activation != $this->request->key) {
			$this->title = "Ошибка при активации";
			$this->meta_desc = "Неверный код активации! Если ошибка будет повторяться, то обратитесь к администрации.";
			$this->meta_key = "активация, ошибка активация, ошибка активация регистрация";
			$hornav->addData("Ошибка активации");
		}
		else {
			$user_db->activation = "";
			try {
				$user_db->save();
			} catch (Exception $e) {print_r($e->getMessage());}
			$this->title = "Ваш аккаунт успешно активирован";
			$this->meta_desc = "Теперь Вы можете войти в свою учётную запись, используя Ваши логин и пароль.";
			$this->meta_key = "активация, успешная активация, успешная активация регистрация";
			$hornav->addData("Активация");
		}
		
		$pm = new PageMessage();
		$pm->hornav = $hornav;
		$pm->header = $this->title;
		$pm->text = $this->meta_desc;
		$this->render($pm);
	}

	public function actionLogout() {
		$user_db = new UserDB;
		$user_db->logout();
		$this->redirect($_SERVER["HTTP_REFERER"]);
	}

	public function actionReset() {
		$message_name = "reset";
		$this->title = "Восстановление пароля";
		$this->meta_desc = "Восстановление пароля пользователя.";
		$this->meta_key = "восстановление пароля, восстановление пароля пользователя";
		$hornav = $this->getHornav();
		$hornav->addData("Восстановление пароля");
		if ($this->request->reset) {
			$user_db = new UserDB();
			$user_db->loadOnEmail($this->request->email);
			if ($user_db->isSaved()) $this->mail->send($user_db->email, array("user" => $user_db, "link" => URL::get("reset", "", array("email" => $user_db->email, "key" => $user_db->getSecretKey()), false, Config::ADDRESS)), "reset");
			$pm = new PageMessage();
			$pm->hornav = $hornav;
			$pm->header = "Восстановление пароля";
			$pm->text = "Инструкция по восстановлению пароля выслана на указанный e-mail адрес.";
			$this->render($pm);
		}
		elseif ($this->request->key) {
			$user_db = new UserDB();
			$user_db->loadOnEmail($this->request->email);
			if ($user_db->isSaved() && ($this->request->key === $user_db->getSecretKey())) {
				if ($this->request->reset_password) {
					$checks = array(array($this->request->password_reset, $this->request->password_reset_conf, "ERROR_PASSWORD_CONF"));
					$user_db = $this->fp->process($message_name, $user_db, array(array("setPassword()", $this->request->password_reset)), $checks);
					if ($user_db instanceof UserDB) {
						$user_db->login();
						$this->redirect(URL::get("sreset"));
					}
				}
				$form = new Form();
				$form->hornav = $hornav;
				$form->header = "Восстановление пароля";
				$form->name = "reset_password";
				$form->action = URL::current();
				$from->message = $this->fp->getSessionMessage($message_name);
				$form->password("password_reset", "Новый пароль:");
				$form->password("password_reset_conf", "Повторите пароль:");
				$form->submit("Далее");
				
				$form->addJSV("password_reset", $this->jsv->password("password_reset_conf"));
				$this->render($form);
			}
			else {
				$pm = new PageMessage();
				$pm->hornav = $hornav;
				$pm->header = "Неверный ключ";
				$pm->text = "Попробуйте ещё раз, если ошибка будет повторяться, то обратитесь к администрации.";
				$this->render($pm);
			}
		}
		else {
			$form = $this->getFormEmail("Восстановление пароля", "reset", $message_name);
			$form->hornav = $hornav;
			$this->render($form);
		}
	}
	
	public function actionSReset() {
		$this->title = "Восстановление пароля";
		$this->meta_desc = "Восстановление пароля успешно завершено.";
		$this->meta_key = "восстановление пароля, восстановление пароля пользователя, восстановление пароля пользователя завершено";
		
		$hornav = $this->getHornav();
		$hornav->addData("Восстановление пароля");
		
		$pm = new PageMessage();
		$pm->hornav = $hornav;
		$pm->header = "Пароль успешно изменён!";
		$pm->text = "Теперь Вы можете войти на сайт, если Вы не авторизовались автоматически.";
		
		$this->render($pm);
	}

	public function actionSearch() {
		$hornav = $this->getHornav();
		$hornav->addData("Поиск");
		$this->title = "Поиск: ".$this->request->query;
		$this->meta_desc = "Поиск ".$this->request->query.".";
		$this->meta_key = "поиск, поиск ".$this->request->query;
		$articles = ArticleDB::search($this->request->query);
		$anecdotes = AnecdoteDB::search($this->request->query);
		// $grammars = AnecdoteDB::search($this->request->query); // This Section Is Not Used
		$dictionarys = DictionaryDB::search($this->request->query);
		$idioms = IdiomDB::search($this->request->query);
		$sr = new SearchResult();
		if (mb_strlen($this->request->query) < Config::MIN_SEARCH_LEN) $sr->error_len = true;
		$sr->hornav = $hornav;
		$sr->field = "full";
		$sr->query = $this->request->query;
		$sr->data_1 = $articles;
		$sr->data_2 = $anecdotes;
		// $sr->data_3 = $grammars; // This Section Is Not Used
		$sr->data_4 = $dictionarys;
		$sr->data_5 = $idioms;
		
		$this->render($sr);
	}
	
	private function getFormEmail($header, $name, $message_name) {
		$form = new Form();
		$form->header = $header;
		$form->name = $name;
		$form->action = URL::current();
		$form->message = $this->fp->getSessionMessage($message_name);
		$form->text("email", "Введите e-mail, указанный при регистрации:", $this->request->email);
		$form->submit("ДАЛЕЕ");
		$form->addJSV("email", $this->jsv->email());
		return $form;
	}
}

?>