<?php

abstract class Controller extends AbstractController {
	
	protected $title;
	protected $meta_desc;
	protected $meta_key;
	protected $mail = null;
	protected $url_active;
	protected $section_id = 0;
	
	public function __construct() {
		parent::__construct(new View(Config::DIR_TMPL), new Message(Config::FILE_MESSAGES));
		$this->mail = new Mail();
		$this->url_active = URL::deleteGET(URL::current(), "page");
	}
	
	public function action404() {
		header("HTTP/1.1 404 Not Found");
		header("Status: 404 Not Found");
		$this->title = "Страница не найдена - 404";
		$this->meta_desc = "Запрошенная страница не существует.";
		$this->meta_key = "страница не найдена, страница не существует, 404";
		
		$pm = new PageMessage();
		$pm->header = "Страница не найдена";
		$pm->text = "К сожалению, запрошенная страница не существует. Проверьте правильность ввода адреса.";
		$this->render($pm);
	}
	
	protected function accessDenied() {
		$this->title = "Доступ закрыт!";
		$this->meta_desc = "Доступ к данной странице закрыт.";
		$this->meta_key = "доступ закрыт, доступ закрыт страница, доступ закрыт страница 403";
		
		$pm = new PageMessage();
		$pm->header = "Доступ закрыт!";
		$pm->text = "У Вас нет прав доступа к данной странице.";
		$this->render($pm);
	}

	final protected function render($str) {
		$params = array();
		$params["header"] = $this->getHeader();
		$params["top"] = $this->getTop();
		$params["searchbt"] = $this->getSearchBlockTop();
		$params["auth"] = $this->getAuth();
		$params["hornav"] = $this->getHornav();
		$params["right"] = $this->getRight();
		$params["center"] = $str;
		$params["footmenu"] = $this->getFootMenu();
		$this->view->render(Config::LAYOUT, $params);
	}

	protected function getHeader() {
		$header = new Header();
		$header->title = $this->title;
		$header->meta("Content-Type", "text/html; charset=utf-8", true);
		$header->meta("description", $this->meta_desc, false);
		$header->meta("keywords", $this->meta_key, false);
		$header->meta("viewport", "width=device-width", false);
		$header->favicon = "/favicon.ico";
		$header->css = array(
			"/styles/main.css"
		);
		$header->js = array(
			"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js",
			"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js",
			"/js/jquery.noty.packaged.min.js",
			"/js/validator.js",
			"/js/functions.js"
		);
		return $header;
	}

	protected function getTop() {
		$items = MenuDB::getTopMenu();
		$topmenu = new TopMenu();
		$topmenu->uri = $this->url_active;
		$topmenu->items = $items;
		return $topmenu;
	}

	protected function getSearchBlockTop() {
		$searchbt = new SearchBlockTop();
		$searchbt->auth_user = $this->auth_user;
		return $searchbt;
	}

	protected function getAuth() {
		if ($this->auth_user) return "";
		$auth = new Auth();
		$auth->message = $this->fp->getSessionMessage("auth");
		$auth->action = URL::current("", true);
		$auth->link_register = URL::get("register");
		$auth->link_reset = URL::get("reset");
		return $auth;
	}

	protected function getRight() {
		if ($this->auth_user) {
			$user_panel = new UserPanel();
			$user_panel->user = $this->auth_user;
			$user_panel->uri = $this->url_active;
			$user_panel->addItem("Панель управления", URL::get("controlpanel", "user"));
			$user_panel->addItem("Редактировать профиль", URL::get("editprofile", "user"));
			$user_panel->addItem("Выход", URL::get("logout"));
		}
		else $user_panel = "";

		$dictionary_word_db = new DictionaryDB();
		$dictionary_word = new DictionaryRandomRight();
		$dictionary_word->dictionarys = DictionaryDB::getAllShowLoadRandom(Config::COUNT_RECORD_ON_RIGHT_BOX, true);
		
		$idiom_word_db = new IdiomDB();
		$idiom_word_db->loadRandom();
		$idiom_word = new IdiomRandomRight();
		$idiom_word->idioms = $idiom_word_db;

		$poll_db = new PollDB();
		$poll_db->loadRandom();
		if ($poll_db->isSaved()) {
			$poll = new Poll();
			$poll->action = URL::get("poll", "", array("id" => $poll_db->id));
			$poll->title = $poll_db->title;
			$poll->data = PollDataDB::getAllOnPollID($poll_db->id);
		}
		else $poll = "";
		return $user_panel.$dictionary_word.$idiom_word.$poll;
	}

	protected function getHornav() {
		$hornav = new Hornav();
		$hornav->addData("Главная", URL::get(""));
		return $hornav;
	}

	protected function getFootmenu() {
		$items = MenuDB::getFootMenu();
		$footmenu = new FootMenu();
		$footmenu->uri = $this->url_active;
		$footmenu->items = $items;
		return $footmenu;
	}

	final protected function getOffset($count_on_page) {
		return $count_on_page * ($this->getPage() - 1);
	}
	
	final protected function getPage() {
		$page = ($this->request->page)? $this->request->page: 1;
		if ($page < 1) $this->notFound();
		return $page;
	}
	
	final protected function getPagination($count_elements, $count_on_page, $url = false) {
		$count_pages = ceil($count_elements / $count_on_page);
		$active = $this->getPage();
		if (($active > $count_pages) && ($active > 1)) $this->notFound();
		$pagination = new Pagination();
		if (!$url) $url = URL::deletePage(URL::current());
		$pagination->url = $url;
		$pagination->url_page = URL::addTemplatePage($url);
		$pagination->count_elements = $count_elements;
		$pagination->count_on_page = $count_on_page;
		$pagination->count_show_pages = Config::COUNT_SHOW_PAGES;
		$pagination->active = $active;
		return $pagination;
	}

	protected function authUser() {
		$login = "";
		$password = "";
		$redirect = false;
		if ($this->request->auth) {
			$login = $this->request->login;
			$password = $this->request->password;
			$redirect = true;
		}
		$user = $this->fp->auth("auth", "UserDB", "authUser", $login, $password);
		if ($user instanceof UserDB) {
			if ($redirect) $this->redirect(URL::current());
			return $user;
		}
		return null;
	}

}
?>