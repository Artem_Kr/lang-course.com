<?php

class Blog extends Module {
	
	public function __construct() {
		parent::__construct();
		$this->add("articles", null, true);
		$this->add("anecdotes", null, true);
	}
	
	public function getTmplFile() {
		return "blog";
	}
	
}

?>