<?php

class MainSection extends Module {
	
	public function __construct() {
		parent::__construct();
		$this->add("sections", null, true);
	}
	
	public function getTmplFile() {
		return "main_section";
	}
	
}

?>