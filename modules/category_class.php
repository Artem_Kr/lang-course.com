<?php

class Category extends Module {
	
	public function __construct() {
		parent::__construct();
		$this->add("section_id");
		$this->add("category");
		$this->add("dictionarys", null, true);
		$this->add("idioms", null, true);
	}
	
	public function getTmplFile() {
		return "category";
	}
	
}

?>