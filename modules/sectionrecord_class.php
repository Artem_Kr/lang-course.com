<?php

class SectionRecord extends Module {
	
	public function __construct() {
		parent::__construct();
		$this->add("articles", null, true);
		$this->add("anecdotes", null, true);
		$this->add("idioms", null, true);
		$this->add("dictionarys", null, true);
		$this->add("categories", null, true);
		$this->add("pagination");
	}
	
	public function getTmplFile() {
		return "section_record";
	}
	
}

?>