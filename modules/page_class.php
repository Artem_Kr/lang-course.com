<?php

class Page extends ModuleHornav {
	
	public function __construct() {
		parent::__construct();
		$this->add("auth_user");
		$this->add("page");
	}
	
	public function getTmplFile() {
		return "page";
	}
	
}

?>