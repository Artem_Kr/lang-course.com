<?php

class Anecdote extends ModuleHornav {
	
	public function __construct() {
		parent::__construct();
		$this->add("auth_user");
		$this->add("anecdote");
		$this->add("prev_anecdote");
		$this->add("next_anecdote");
	}
	
	public function getTmplFile() {
		return "anecdote";
	}
	
}

?>