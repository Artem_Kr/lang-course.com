<?php

class Intro extends ModuleHornav {
	
	public function __construct() {
		parent::__construct();
		$this->add("obj");
		$this->add("search_form");
	}
	
	public function getTmplFile() {
		return "intro";
	}
	
}

?>