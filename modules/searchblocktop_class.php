<?php

class SearchBlockTop extends Module {
	
	public function __construct() {
		parent::__construct();
		$this->add("auth_user");
		$this->add("link_search", URL::get("search"));
	}
	
	public function getTmplFile() {
		return "searchblocktop";
	}
	
}

?>