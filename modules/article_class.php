<?php

class Article extends ModuleHornav {
	
	public function __construct() {
		parent::__construct();
		$this->add("auth_user");
		$this->add("article");
		$this->add("prev_article");
		$this->add("next_article");
	}
	
	public function getTmplFile() {
		return "article";
	}
	
}

?>