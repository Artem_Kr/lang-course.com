<?php

class CategoryDB extends ObjectDB {
	
	protected static $table = "categories";
	
	public function __construct() {
		parent::__construct(self::$table);
		$this->add("title", "ValidateTitle");
		$this->add("main_section_id", "ValidateID");
		$this->add("section_id", "ValidateID");
		$this->add("description", "ValidateText");
		$this->add("meta_desc", "ValidateMD");
		$this->add("meta_key", "ValidateMK");
	}
	
	protected function postInit() {
		$this->link = URL::get("category", "", array("id" => $this->id));
		$section = new SectionDB();
		$section->load($this->section_id);
		$this->section = $section;
		return true;
	}

	public static function getAllOnSectionID($section_id) {
		return self::getAllOnCategory("section_id", $section_id);
	}

	private static function getAllOnCategory($field, $value) {
		$select = self::getBaseSelect();
		$select->where("`$field` = ".self::$db->getSQ(), array($value))
			->order("id");
		$data = self::$db->select($select);
		$sections = ObjectDB::buildMultiple(__CLASS__, $data);
		return $sections;
	}

	private static function getBaseSelect() {
		$select = new Select(self::$db);
		$select->from(self::$table, "*");
		return $select;
	}
	
}

?>