<?php

class DictionaryDB extends ObjectDB {
	
	protected static $table = "dictionarys";
	
	public function __construct() {
		parent::__construct(self::$table);
		$this->add("title", "ValidateTitle");
		$this->add("full", "ValidateText");
		$this->add("main_section_id", "ValidateID");
		$this->add("section_id", "ValidateID");
		$this->add("cat_id", "ValidateID");
		$this->add("date", "ValidateDate", self::TYPE_TIMESTAMP, $this->getDate());
	}
	
	protected function postInit() {
		$this->link = URL::get("dictionary", "", array("id" => $this->id));
		return true;
	}
	
	protected function postLoad() {
		$this->postHandling();
		return true;
	}
	
	public static function getAllShow($count = false, $offset = false, $post_handling = false) {
		$select = self::getBaseSelect();
		$select->order("date", false);
		if ($count) $select->limit($count, $offset);
		$data = self::$db->select($select);
		$dictionarys = ObjectDB::buildMultiple(__CLASS__, $data);
		if ($post_handling) foreach ($dictionarys as $dictionary) $dictionary->postHandling();
		return $dictionarys;
	}

	public static function getAllShowLoadRandom($count = false, $post_handling = false) {
		$select = new Select(self::$db);
		$select->from(self::$table, "*")
			->rand()
			->limit(1);
		if ($count) $select->limit($count);
		$data = self::$db->select($select);
		$dictionarys = ObjectDB::buildMultiple(__CLASS__, $data);
		if ($post_handling) foreach ($dictionarys as $dictionary) $dictionary->postHandling();
		return $dictionarys;
	}
	
	public static function getAllOnPageAndCategoryID($cat_id, $count, $offset = false) {
		$select = self::getBaseSelect();
		$select->order("date", false)
			->where("`cat_id` = ".self::$db->getSQ(), array($cat_id))
			->rand()
			->limit($count, $offset);
		$data = self::$db->select($select);
		$dictionarys = ObjectDB::buildMultiple(__CLASS__, $data);
		foreach ($dictionarys as $dictionary) $dictionary->postHandling();
		return $dictionarys;
	}
	
	public function search($words) {
		$select = self::getBaseSelect();
		$dictionarys = self::searchObjects($select, __CLASS__, array("title", "full"), $words, Config::MIN_SEARCH_LEN);
		foreach ($dictionarys as $dictionary) $dictionary->setSectionAndCategory();
		return $dictionarys;
	}
	
	private static function getBaseNeighbourSelect($dictionary_db) {
		$select = self::getBaseSelect();
		$select->where("`cat_id` = ".self::$db->getSQ(), array($dictionary_db->cat_id))
			->order("date")
			->limit(1);
		return $select;
	}
	
	private static function getBaseSelect() {
		$select = new Select(self::$db);
		$select->from(self::$table, "*");
		return $select;
	}
	
	private function setSectionAndCategory() {
		$main_section = new MainSectionDB();
		$main_section->load($this->main_section_id);
		$section = new SectionDB();
		$section->load($this->section_id);
		$category = new CategoryDB();
		$category->load($this->cat_id);
		if ($main_section->isSaved()) $this->main_section = $main_section;
		if ($section->isSaved()) $this->section = $section;
		if ($category->isSaved()) $this->category = $category;
		
	}
	
	private function postHandling() {
		$this->setSectionAndCategory();
	}
	
}

?>