<?php

class PageDB extends ObjectDB {
	
	protected static $table = "other_pages";
	
	public function __construct() {
		parent::__construct(self::$table);
		$this->add("title", "ValidateTitle");
		$this->add("full", "ValidateText");
		$this->add("date", "ValidateDate", self::TYPE_TIMESTAMP, $this->getDate());
		$this->add("meta_desc", "ValidateMD");
		$this->add("meta_key", "ValidateMK");
	}
	
	protected function postInit() {
		$this->link = URL::get("page", "", array("id" => $this->id));
		return true;
	}

	public static function getAllShow($count = false, $offset = false) {
		$select = self::getBaseSelect();
		$select->order("date", false);
		if ($count) $select->limit($count, $offset);
		$data = self::$db->select($select);
		$pages = ObjectDB::buildMultiple(__CLASS__, $data);
		return $pages;
	}
	
	public function search($words) {
		$select = self::getBaseSelect();
		$pages = self::searchObjects($select, __CLASS__, array("title", "full"), $words, Config::MIN_SEARCH_LEN);
		return $pages;
	}
	
	private static function getBaseSelect() {
		$select = new Select(self::$db);
		$select->from(self::$table, "*");
		return $select;
	}
	
}

?>