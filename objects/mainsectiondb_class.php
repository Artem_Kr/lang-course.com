<?php

class MainSectionDB extends ObjectDB {
	
	protected static $table = "main_sections";
	
	public function __construct() {
		parent::__construct(self::$table);
		$this->add("title", "ValidateTitle");
		$this->add("meta_desc", "ValidateMD");
		$this->add("meta_key", "ValidateMK");
	}
	
	protected function postInit() {
		$this->link = URL::get("mainsection", "", array("id" => $this->id));
		return true;
	}
	
}

?>