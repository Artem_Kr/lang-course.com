<?php

class AnecdoteDB extends ObjectDB {
	
	protected static $table = "anecdotes";
	
	public function __construct() {
		parent::__construct(self::$table);
		$this->add("title", "ValidateTitle");
		$this->add("full", "ValidateText");
		$this->add("main_section_id", "ValidateID");
		$this->add("section_id", "ValidateID");
		$this->add("cat_id", "ValidateID");
		$this->add("date", "ValidateDate", self::TYPE_TIMESTAMP, $this->getDate());
		$this->add("meta_desc", "ValidateMD");
		$this->add("meta_key", "ValidateMK");
	}
	
	protected function postInit() {
		$this->link = URL::get("anecdote", "", array("id" => $this->id));
		return true;
	}
	
	protected function postLoad() {
		$this->postHandling();
		return true;
	}
	
	public static function getAllShow($count = false, $offset = false, $post_handling = false) {
		$select = self::getBaseSelect();
		$select->order("date", false);
		if ($count) $select->limit($count, $offset);
		$data = self::$db->select($select);
		$anecdotes = ObjectDB::buildMultiple(__CLASS__, $data);
		if ($post_handling) foreach ($anecdotes as $anecdote) $anecdote->postHandling();
		return $anecdotes;
	}
	
	public static function getAllOnPageAndSectionID($section_id, $count, $offset = false) {
		$select = self::getBaseSelect();
		$select->order("date", false)
			->where("`section_id` = ".self::$db->getSQ(), array($section_id))
			->limit($count, $offset);
		$data = self::$db->select($select);
		$anecdotes = ObjectDB::buildMultiple(__CLASS__, $data);
		foreach ($anecdotes as $anecdote) $anecdote->postHandling();
		return $anecdotes;
	}
	
	public static function getAllOnSectionID($section_id, $order = false, $offset = false) {
		return self::getAllOnSectionOrCategory("section_id", $section_id, $order, $offset);
	}
	
	public static function getAllOnCatID($cat_id, $order = false, $offset = false) {
		return self::getAllOnSectionOrCategory("cat_id", $cat_id, $order, $offset);
	}
	
	private static function getAllOnSectionOrCategory($field, $value, $order, $offset) {
		$select = self::getBaseSelect();
		$select->where("`$field` = ".self::$db->getSQ(), array($value))
			->order("date", $order);
		$data = self::$db->select($select);
		$anecdotes = ObjectDB::buildMultiple(__CLASS__, $data);
		return $anecdotes;
	}
	
	public function loadPrevAnecdote($anecdote_db) {
		$select = self::getBaseNeighbourSelect($anecdote_db);
		$select->where("`id` < ".self::$db->getSQ(), array($anecdote_db->id))
			->order("date", false);
		$row = self::$db->selectRow($select);
		return $this->init($row);
	}
	
	public function loadNextAnecdote($anecdote_db) {
		$select = self::getBaseNeighbourSelect($anecdote_db);
		$select->where("`id` > ".self::$db->getSQ(), array($anecdote_db->id));
		$row = self::$db->selectRow($select);
		return $this->init($row);
	}
	
	public function search($words) {
		$select = self::getBaseSelect();
		$anecdotes = self::searchObjects($select, __CLASS__, array("title", "full"), $words, Config::MIN_SEARCH_LEN);
		foreach ($anecdotes as $anecdote) $anecdote->setSectionAndCategory();
		return $anecdotes;
	}
	
	private static function getBaseNeighbourSelect($anecdote_db) {
		$select = self::getBaseSelect();
		$select->where("`section_id` = ".self::$db->getSQ(), array($anecdote_db->section_id))
			->order("date")
			->limit(1);
		return $select;
	}
	
	private static function getBaseSelect() {
		$select = new Select(self::$db);
		$select->from(self::$table, "*");
		return $select;
	}
	
	private function setSectionAndCategory() {
		$main_section = new MainSectionDB();
		$main_section->load($this->main_section_id);
		$section = new SectionDB();
		$section->load($this->section_id);
		$category = new CategoryDB();
		$category->load($this->cat_id);
		if ($main_section->isSaved()) $this->main_section = $main_section;
		if ($section->isSaved()) $this->section = $section;
		if ($category->isSaved()) $this->category = $category;
		
	}
	
	private function postHandling() {
		$this->setSectionAndCategory();
	}
	
}

?>