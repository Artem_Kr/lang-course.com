<?php

class SectionDB extends ObjectDB {
	
	protected static $table = "sections";
	
	public function __construct() {
		parent::__construct(self::$table);
		$this->add("title", "ValidateTitle");
		$this->add("main_section_id", "ValidateID");
		$this->add("description", "ValidateText");
		$this->add("meta_desc", "ValidateMD");
		$this->add("meta_key", "ValidateMK");
	}
	
	protected function postInit() {
		$this->link = URL::get("section", "", array("id" => $this->id));
		$mainsection = new MainSectionDB();
		$mainsection->load($this->main_section_id);
		$this->mainsection = $mainsection;
		return true;
	}

	public static function getAllOnMainSectionID($main_section_id) {
		return self::getAllOnSection("main_section_id", $main_section_id);
	}

	private static function getAllOnSection($field, $value) {
		$select = self::getBaseSelect();
		$select->where("`$field` = ".self::$db->getSQ(), array($value))
			->order("id");
		$data = self::$db->select($select);
		$mainsections = ObjectDB::buildMultiple(__CLASS__, $data);
		return $mainsections;
	}

	private static function getBaseSelect() {
		$select = new Select(self::$db);
		$select->from(self::$table, "*");
		return $select;
	}
	
}

?>