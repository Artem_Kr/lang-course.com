<?php

abstract class ObjectDB extends AbstractObjectDB {
	
	private static $months = array(
		"января",
		"февраля",
		"марта",
		"апреля",
		"мая",
		"июня",
		"июля",
		"августа",
		"сентября",
		"октября",
		"ноября",
		"декабря"
	);
	
	public function __construct($table) {
		parent::__construct($table, Config::FORMAT_DATE);
	}
	
	
	protected static function getDay($date = false) {
		if ($date) $date = strtotime($date);
		return date("j", $date);
	}
	
	protected static function getMonth($date = false) {
		if ($date) $date = strtotime($date);
		else $date = time();
		return self::$months[date("n", $date) - 1];
	}
	
	protected static function getYear($date = false) {
		if ($date) $date = strtotime($date);
		return date("Y", $date);
	}
	
	public function preEdit($field, $value) {
		return true;
	}
	
	public function postEdit($field, $value) {
		return true;
	}
	
	public function accessEdit($auth_user, $field) {
		return false;
	}
	
	public function accessDelete($auth_user) {
		return false;
	}
	
}

?>