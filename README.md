	const SITENAME = "Lang-Course.com";
	const SECRET = "DGLJDG5";
	const ADDRESS = "http://lang-course.local";
	const ADM_NAME = "Крынкин Артем";
	const ADM_EMAIL = "admin@lang-course.com";

	const DB_HOST = "localhost";
	const DB_USER = "root";
	const DB_PASSWORD = "root";
	const DB_NAME = "langcouse";
	const DB_PREFIX = "xyz_";
	const DB_SYM_QUERY = "?";

	const DIR_IMG = "/images/";
	const DIR_IMG_ARTICLES = "/frontend/web/images/articles/";
	const DIR_AVATAR = "/frontend/web/images/avatars/";
	const DIR_TMPL = "/Applications/MAMP/htdocs/MyProgect/lang-course.com/frontend/views/";
	const DIR_EMAILS = "/Applications/MAMP/htdocs/MyProgect/lang-course.com/tmpl/emails/";

	const LAYOUT_S = "site/layout/main";
	const LAYOUT_C = "controlpanel/layout/main";
	
	const FILE_MESSAGES = "/Applications/MAMP/htdocs/MyProgect/lang-course.com/text/messages.ini";

	const FORMAT_DATE = "%d.%m.%Y %H:%M:%S";

	const COUNT_RECORD_ON_RIGHT_BOX = 2;
	const COUNT_RECORD_ON_PAGE_INDEX = 3;
	const COUNT_TEN_RECORD_ON_PAGE = 10;
	const COUNT_FIFTEEN_RECORD_ON_PAGE = 15;
	const COUNT_SHOW_PAGES = 10;

	const MIN_SEARCH_LEN = 3;
	const LEN_SEARCH_RES = 255;

	const SEF_SUFFIX = ".html";

	const DEFAULT_AVATAR = "default.png";
	const MAX_SIZE_AVATAR = 61440;